load 'stages.groovy'

REF=HG19

TMPDIR="./tmp"

run {
    ~"(.*)_R[0-9][_.].*fastq.gz" *  [ align_bwa ] + merge_bams
}
