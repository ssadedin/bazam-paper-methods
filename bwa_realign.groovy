load 'stages.groovy'

source_type='bam'

if(args.any { it.endsWith('.cram') })
    source_type = 'cram'

run { 
  "%.$source_type" * [ (1..bwa_parallelism) * [ realign ] + merge_bams + index_bam ]
}
