# bazam-paper-scripts

Scripts and other resources related to the Bazam manuscript.

Most of the analyses for the manuscript involved multistage pipelines processing whole 
genome data. To handle this, analysis was done using the Bpipe (http://bpipe.org) 
workflow tool.

See pipeline scripts in and corresponding command logs and bpipe history files
which are saved by bpipe in each corresponding subdirectories representing part 
of the analysis:
 
 * Comparison to check FASTQ from Bazam is identical to original:  [compare_fastq](compare_fastq)
 * Comparison to see alignment differences between non-Bazam and bazam: [hg19_samtools_to_hg19_bazam](hg19_samtools_to_hg19_bazam)
 * Test of time to realign hg19 to GRch38 using Bazam: [hg19_to_grch38](hg19_to_grch38)
 * Test of time to realign hg19 to GRch38 using Bazam Sharded: [hg19_to_grch38_shard](hg19_to_grch38_shard)
 * Test of time to realign using Picard SamtoFastq: [hg19_to_hg19_picard](hg19_to_hg19_picard)
 * Test of time to realign using Samtools bamshuf / short-extract-realign : [hg19_to_grch38_samtools](hg19_to_grch38_samtools)

Note that to actually execute these steps you will need to download and setup all the associated tools including:

 * BWA
 * SAMTools
 * Picard Tools
 * Reference Genomes (GRCh37, GRCh38)
 * The execution environment (cluster, standalone server etc)

See the [bpipe.config](bpipe.config) file for variables to set to customize these things.
