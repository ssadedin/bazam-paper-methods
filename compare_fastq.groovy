load 'stages.groovy'

REF=GRCH37

run {
    '%.bam' * [ 
        init_sample + 
        extract_all_ser + 
        align_bwa.using(interleaved: true) +
        index_bam + 
        extract_all_bazam + 
        find_missing
     ]
}
