/**************************************************************
 * Bazam Manuscript Analysis Pipeline Stages
 *
 * These stages represent the commands used in generating results for
 * the manuscript describing Bazam, see https://github.com/ssadedin/bazam
 *
 */
import gngs.*

BWA="bwa"
source_type='bam'

init_sample = {
    branch.sample = new SAM(file(input.bam)).samples[0]
}

realign = {

    doc "Realign BAM file using Bazam with customisable reference, sharding parameters and bwa threads"

    var REF : GRCH38,
        BWA_THREADS:12

    branch.shard = branch.name

    String shardFlag = bwa_parallelism > 1 ? "-s $shard,$bwa_parallelism" : ""

    println "BWA Parallelism = $bwa_parallelism, Shard flag = $shardFlag"

    System.setProperty('samjdk.reference_fasta', CRAM_REF)

    def sample = new SAM(file(input[source_type])).samples[0]

    produce(([sample] + (bwa_parallelism>1?[shard]:[]) +  ['realign.bam']).join('.')) {
        exec """
            export JAVA_OPTS="-Dsamjdk.reference_fasta=$CRAM_REF" 

            java -Xmx16g -Dreference_fasta=$CRAM_REF -Dsamjdk.reference_fasta=$CRAM_REF 
                 -cp $GNGS_JAR:$BAZAM/build/libs/bazam.jar bazam.Bazam 
                 -n 6   $shardFlag
                 -namepos
                 -bam ${input[source_type]} |  
            bwa mem -p -M -t $BWA_THREADS
                 $REF
                 -R "@RG\\tID:$sample\\tPL:Illumina\\tPU:NA\\tLB:L001\\tSM:$sample" - |  
            samtools view -bSuh - | samtools sort -o $output.bam -T $output.bam.prefix

        """, "bazam"
    }
}


merge_bams = {

    if(bwa_parallelism == 1) {
        println "Skipping merge because bwa parallelism not in use"
        return
    }

    produce(file(input.bam).name.replaceAll('[0-9]*.realign.bam', 'merge.bam')) {
        exec """
            time java -Xmx2g -jar $STRETCH/tools/picard/2.2.1/picard.jar MergeSamFiles
                ${inputs.bam.withFlag("INPUT=")}
                VALIDATION_STRINGENCY=LENIENT
                ASSUME_SORTED=true
                CREATE_INDEX=true
                OUTPUT=$output.bam
         """, "merge"
    }

}

samtools_realign = {

    var REF : GRCH38,
        sample: new SAM(input.bam.toString()).samples[0]

    exec """
        samtools bamshuf -uO $input.bam ${file(input.bam.prefix).name}-tmp | 
            samtools bam2fq --threads 4 -s $output.se.fq.gz - | 
            bwa mem -p -M -t 12 $REF -R "@RG\\tID:$sample\\tPL:Illumina\\tPU:NA\\tLB:L001\\tSM:$sample" - |  
            samtools view -bSuh - | 
            samtools sort -o $output.bam -T $output.bam.prefix
    ""","samtools_realign"
}

align_bwa = {

    doc "Align with bwa mem algorithm."

    requires REF : 'The reference to align to'

    var seed_length : 19,
        library : 'Unknown',
        PLATFORM: 'Illumina',
        sample: file(inputs.gz).name.tokenize('_')[0],
        interleaved: false
    
    def fastq_inputs = inputs.gz
    def interleavedOpt = ""
    if(interleaved) {
        interleavedOpt = "-p"
    }

    println "FASTQ inputs are $inputs.gz"

    def lanes = ['L001']; // inputs.gz.collect { (it.toString() =~ /_(L[0-9]{1,3})_/)[0][1] }.unique()
       
    branch.lane = lanes[0]

    def outputFile = sample + "_" + gngs.Hash.sha1(inputs.gz*.toString().join(",")) + "_" + lane + ".bam"

    List fastqInputs 
    if(interleaved) {
        fastqInputs = [input.gz]
    }
    else {
        fastqInputs = inputs.gz as List
    }

    // var BWA_THREADS: false;

    if(!BWA_THREADS) {
        BWA_THREADS = 1
    }

    from(fastqInputs) produce(outputFile) {
        
        //    Note: the results are filtered with flag 0x100 because bwa mem includes multiple 
        //    secondary alignments for each read, which can upset downstream tools such as 
        //    GATK and Picard.
        def safe_tmp_dir = ['./tmpdata', UUID.randomUUID().toString()].join( File.separator )
        exec """
                set -o pipefail

                mkdir -p "$safe_tmp_dir"

                bwa mem -M -t $threads -k $seed_length $interleavedOpt
                        -R "@RG\\tID:${sample}_${lane}\\tPL:$PLATFORM\\tPU:1\\tLB:${library}\\tSM:${sample}"  
                        $REF $inputs.gz | 
                        samtools view -F 0x100 -bSu - | samtools sort -o ${output.prefix}.bam -T "$safe_tmp_dir/bamsort"

                rm -r "$safe_tmp_dir"
        ""","bwamem"
    }
}

index_bam = {
    transform('bam') to('bam.bai') {
        exec """samtools index $input.bam"""
    }

    forward input.bam
}

extract_all_ser = {

    doc "Extracts all the reads to a FASTQ file using standard tools and the Sort-Extract process"

    produce(branch.sample + '.fastq.gz', branch.sample + '.se.fastq.gz') {
        exec """                                                                      
            samtools bamshuf -uO $input.bam ${file(input.bam.prefix).name}-tmp | 
            samtools bam2fq -s $output2.gz - | gzip -c > $output1.gz
        ""","extract_all_ser"
    }
    branch.fastq_ser = output1.gz
}

extract_all_bazam = {

    doc "Extracts all the reads to a FASTQ file using Bazam"

    transform('bazam.fastq.gz') {
        exec """
            java -Xmx16g -cp $GNGS_JAR:$BAZAM/build/libs/bazam.jar bazam.Bazam -bam $input.bam | gzip -c > $output.gz
        """, "extract_all_bazam"
    }

    branch.fastq_bazam = output.gz

}

find_missing = {

    doc """
        Compares to FASTQ files to find reads that are in one of them but not the other, based on comparison 
        of read name and base call content
        """


    from(fastq_ser, fastq_bazam) {
        exec """
            set -o pipefail

            java -Xmx32g -cp $GNGS_JAR:$BAZAM/build/libs/bazam.jar  gngs.tools.FastQIdent -f1 $input1.gz -f2 $input2.gz | gzip -c > ${output('bazam_extra.txt.gz')} &

            java -Xmx32g -cp $GNGS_JAR:$BAZAM/build/libs/bazam.jar  gngs.tools.FastQIdent -f1 $input2.gz -f2 $input1.gz | gzip -c > ${output('bazam_missing.txt.gz')} 

        """, "find_missing_fastq"
    }
}

picard_realign = {

    doc """
        Implements realignment using Picard SamtoFastq.

        Note that this command actually fails on the evaluation data set due to anomalies in the data
        """

    var REF : GRCH38,
        BWA_THREADS:12

    System.setProperty('samjdk.reference_fasta', CRAM_REF)

    def sample = new SAM(file(input[source_type])).samples[0]


    exec """
         java -Xmx32g -jar $PICARD/picard.jar SamToFastq 
            INTERLEAVE=true 
            VALIDATION_STRINGENCY=LENIENT 
            I=$input.bam 
            F=/dev/stdout | 
         bwa mem -p -M -t $BWA_THREADS
            $REF
            -R "@RG\\tID:$sample\\tPL:Illumina\\tPU:NA\\tLB:L001\\tSM:$sample" - |  
            samtools view -bSuh - | samtools sort -o $output.bam -T $output.bam.prefix
    ""","picard_realign"
}


