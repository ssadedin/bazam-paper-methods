load 'stages.groovy'

source_type='bam'

if(args.any { it.endsWith('.cram') })
    source_type = 'cram'

run { 
  "%.$source_type" * [ picard_realign ] + index_bam 
}
