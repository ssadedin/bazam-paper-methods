/**
 * Script to count the number of reads that moved position by scanning a BAM file and
 * checking the read names to see if their position matches the position encoded
 * in the read name
 */

import gngs.*
import htsjdk.samtools.SAMRecord

int written = 0
int total = 0

ProgressCounter progress = new ProgressCounter(withTime:true, withRate:true, extra: {"Written: $written, ${Utils.perc(written/(1.0d + total))}"})

new SAM(args[0]).filter('movedReads.bam') { SAMRecord r ->

  progress.count()
  ++total

  try {

      List parts = r.readName.tokenize(':')[7..-1]

      String oldPos
      if(r.firstOfPairFlag) {
         oldPos = parts.size() == 3 ? (parts[-3]+':'+parts[-2]) : (parts[-4] + ':' + parts[-3])
      }
      else {
         oldPos = parts.size() == 3 ? (parts[-3]+':'+parts[-1]) : (parts[-2] + ':' + parts[-1])
      }

      // println r.readName + ' => ' + oldPos

      String newPos = r.referenceName + ':' + r.alignmentStart

      boolean include = (oldPos != newPos)

      if(include)
        ++written

      return include
  }
  catch(Exception e) {
    println "Error processing $r.readName"
    e.printStackTrace()
  }

}

progress.end()
